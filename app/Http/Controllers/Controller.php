<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getData(){
        $datas = Storage::get('list.json');
        $datas = json_decode($datas);
        $now = Carbon::now('Asia/Bangkok');

        foreach($datas as $index => $data){
            $date = Carbon::parse($data->created_at, 'Asia/Bangkok');
            $data->created_at = (String)$now->diffInWeeks($date) . ' weeks ago';
            $data->price = '฿' . $data->price;
        }

        return view('challenges')
                ->with('datas', $datas);
    }
}
